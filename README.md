# Amazon SageMaker Crime Prediction Model

## Project Summary
The aim of this project is to use SageMaker’s [Linear Learner](https://docs.aws.amazon.com/sagemaker/latest/dg/linear-learner.html) algorithm to train a linear model for crime prediction. For this illustration, linear binary classifier was used and stop-and-search crime data was pulled from [data.police.uk](https://data.police.uk/) data set available at [https://data.police.uk/data/](https://data.police.uk/data/). 

**[data.police.uk](https://data.police.uk/) is a site for open data about crime and policing in England, Wales and Northern Ireland.**



The purpose here is to use this data set to build a predictive policing model to determine whether or not crime is likely given the following data points:

* Location
* Age
* Gender
* Time of Day
* Day of Week
* Month

Crime is predicted when an HTTP POST is made to the SageMaker model endpoint.

## Table of Contents


## Architecture Diagram
![alt text](https://s3.amazonaws.com/udacity-website/SageMakerPublic.png "SageMaker Archi Diagram")

| Component     | Detail        | 
| ------------- |:-------------:| 
| API Gateway   | Service used to create secure APIs. API Gateway passes the user's request to the Lambda. | 
| Lambda        | Lambda authored in Python that invokes the model endpoint with user's request.      |  
| SageMaker     | Machine Learning service used to train and host crime predicting model.|   


## Model Overview
The model returns a 'Crime' or 'No Crime' prediction based on the input provided. The crime model does a good job at predicting crime and has an overall accuracy score of 86%.


## Model Endpoint

A POST request can be sent to the model endpoint `https://ejcq4vf5fl.execute-api.us-east-1.amazonaws.com/test/predictcrime`.

The API Key`bu9o0EUWEN56aUbaN2VIF3kvVUeqUp0r8KbAz0s4` should be sent as a request header called `x-api-key`.


### Request Structure

#### Sample 1
##### JSON Format: 
`{"data":"1,3,21,0,1,0,0,1,0,0,0,1,0,0,0"}`

##### Description:

| DayofWeek|Month|AverageAge|Gender_Female|Gender_Male|TimeofDay_afternoon| TimeofDay_evening|TimeofDay_morning|County_DevonCornwall|County_Dorset|County_Essex|County_Hampshire|County_Kent|County_Nottinghamshire|County_Surrey| 
| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |
| 1|3|21|0|1|0|0|1|0|0|0|1|0|0|0|

#### Sample 2
##### JSON Format: 
`{"data":"4,10,30,0,1,1,0,0,0,0,0,0,0,1,0"}`

##### Description:

| DayofWeek|Month|AverageAge|Gender_Female|Gender_Male|TimeofDay_afternoon| TimeofDay_evening|TimeofDay_morning|County_DevonCornwall|County_Dorset|County_Essex|County_Hampshire|County_Kent|County_Nottinghamshire|County_Surrey| 
| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |:-------------:| ------------- |
| 4|10|30|0|1|1|0|0|0|0|0|0|0|1|0|

### Response 
"No Crime" or "Crime"

## Postman Example
![alt text](https://s3.amazonaws.com/udacity-website/postman.png "Postman Sample Call")


## Code Samples
* Jupyter Notebook `CrimePredictionModel.ipynb`

## Author
[Kesha Williams](https://www.linkedin.com/in/java-rock-star-kesha/)

## Disclaimer
* **This model was created solely for academic purposes and should not be used in a production environment.**
* **There is a strict usage plan that allows for only a limited amount of requests to the endpoint per month.**
